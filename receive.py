#!venv/bin/python

import pika

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='commands')

hitpoints = 10

def callback(ch, method, properties, body):
    global hitpoints
    theBody = body.decode('utf-8')
    if theBody == "attack":
        hitpoints -= 1
        print(" you attack; hitpoints are %r" % hitpoints)
    else:
        print(" invalid command %r" % theBody)

channel.basic_consume(callback,
                      queue='commands',
                      no_ack=True)

print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()
