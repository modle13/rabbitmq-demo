#!venv/bin/python

import pika
import time

connection = pika.BlockingConnection(pika.ConnectionParameters('localhost'))
channel = connection.channel()

channel.queue_declare(queue='commands')

x = 0
while x < 10:
    channel.basic_publish(exchange='',
                      routing_key='commands',
                      body='attack')
    print(" [x] Sent 'attack'")
    x += 1
    time.sleep(1)

connection.close()
