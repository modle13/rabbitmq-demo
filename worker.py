#!venv/bin/python

import pika
import time

connection = pika.BlockingConnection(pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='task_queue', durable=True)
print(' [*] Waiting for messages. To exit press CTRL+C')

hitpoints = 10

def callback(ch, method, properties, body):
    global hitpoints
    theBody = body.decode('utf-8')
    print(" processing command %r" % theBody)
    time.sleep(body.count(b'.'))
    if theBody == "attack":
        hitpoints -= 1
        print(" you attack; hitpoints are %r" % hitpoints)
    elif theBody == "look":
        print(" you look around the room")
    else:
        print(" invalid command %r" % theBody)
    print " Done"
    ch.basic_ack(delivery_tag = method.delivery_tag)

channel.basic_qos(prefetch_count=1)
channel.basic_consume(callback,
                      queue='task_queue')

channel.start_consuming()
